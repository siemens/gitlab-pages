BINDIR := $(CURDIR)/bin
GO_BUILD_TAGS   := continuous_profiler_stackdriver
MOCKGEN_VERSION=v1.6.0
FIPS_MODE       ?= 0
ifeq ($(FIPS_MODE), 1)
    BORINGCRYPTO_SUPPORT := $(shell GOEXPERIMENT=boringcrypto go version > /dev/null 2>&1; echo $$?)
    ifeq ($(BORINGCRYPTO_SUPPORT), 0)
        GO_BUILD_ENV=GOEXPERIMENT=boringcrypto
    endif

    GO_BUILD_TAGS := $(GO_BUILD_TAGS),fips
endif

.PHONY: all generate-mocks build clean

all: gitlab-pages

generate-mocks:
	$Q go run github.com/golang/mock/mockgen@$(MOCKGEN_VERSION) -source=internal/interface.go -destination=internal/handlers/mock/handler_mock.go -package=mock
	$Q go run github.com/golang/mock/mockgen@$(MOCKGEN_VERSION) -source=internal/source/source.go -destination=internal/source/mock/source_mock.go -package=mock
	$Q go run github.com/golang/mock/mockgen@$(MOCKGEN_VERSION) -source=internal/source/gitlab/mock/client_stub.go -destination=internal/source/gitlab/mock/client_mock.go -package=mock
	$Q go run github.com/golang/mock/mockgen@$(MOCKGEN_VERSION) -source=internal/domain/resolver.go -destination=internal/domain/mock/resolver_mock.go -package=mock

build:
	$Q GOBIN=$(BINDIR) $(GO_BUILD_ENV) go install $(if $V,-v) -ldflags="$(VERSION_FLAGS) -B gobuildid" -tags "${GO_BUILD_TAGS}" -buildmode exe $(IMPORT_PATH)
ifeq ($(FIPS_MODE), 1)
	go tool nm $(BINDIR)/gitlab-pages | grep FIPS >/dev/null &&  echo "binary is correctly built in FIPS mode" || (echo "binary is not correctly built in FIPS mode" && exit 1)
endif

clean:
	$Q GOBIN=$(BINDIR) go clean -i -modcache -x

gitlab-pages: build
	$Q cp -f $(BINDIR)/gitlab-pages .
